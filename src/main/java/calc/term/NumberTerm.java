package calc.term;

import java.io.IOException;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.TokenNotFoundException;

class NumberTerm extends Term {
  private CalcValue value;

  NumberTerm(CalcValue aValue) {
    value = aValue;
  }

  NumberTerm(CalcReader reader) throws IOException, ParseErrorException, TokenNotFoundException {
    value = new CalcValue(reader);
  }

  public CalcValue getValue() {
    return value;
  }

}
