package calc.term;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;


public interface TermParserInterface {

  Term parse(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException;

}
