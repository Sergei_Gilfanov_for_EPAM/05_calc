package calc.term;

import calc.CalcValue;

/*
 * Абстракция для представления операндов операторов
 * 
 */
public abstract class Term {
  abstract public CalcValue getValue();
}
