package calc.term;

import java.io.IOException;
import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.expr.Expr;

class ParenthesesTerm extends Term {
  private Expr expr;
  
  ParenthesesTerm(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    
    int c = reader.skipWhitespacesRead();
    if (c != '(') {
      reader.unread(c);
      throw new TokenNotFoundException();
    }

    try {
      expr = parser.expr().parse(reader, parser);
    } catch (TokenNotFoundException exp) {
      throw new ParseErrorException("Ожидается непустое выражение");
    }

    c = reader.skipWhitespacesRead();
    if (c != ')') {
      reader.unread(c);
      throw new ParseErrorException("Ожидается ')'");
    }
  }


  public CalcValue getValue() {
    return expr.getValue();
  }

}
