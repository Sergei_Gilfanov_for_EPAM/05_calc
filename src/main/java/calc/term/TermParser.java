package calc.term;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;

public class TermParser implements TermParserInterface {
  /* (non-Javadoc)
   * @see calc.term.TermParserInterface#parse(calc.CalcReader)
   */
  @Override
  public Term parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c = reader.skipWhitespacesRead();
    reader.unread(c);
    if (c == '(') {
      return new ParenthesesTerm(reader, parser);
    }
    return new NumberTerm(reader);
  }
}
