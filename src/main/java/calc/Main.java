package calc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import calc.expr.Expr;

public class Main {

  public static void main(String[] args) throws IOException {
    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
    Parser parser = new Parser();

    String line;
    while ((line = bufferRead.readLine()) != null) {
      CalcReader reader = new CalcReader(line);
      Expr expr;
      try {
        expr = parser.expr().parse(reader, parser);
      } catch (TokenNotFoundException exp) {
        String errorContext = reader.getContext();
        String outputMessage = String.format("Пустое выражение после '%s'", errorContext);
        System.out.println(outputMessage);
        continue;
      } catch (ParseErrorException exp) {
        String errorMessage = exp.getMessage();
        String errorContext = reader.getContext();
        String outputMessage = String.format("%s после '%s'", errorMessage, errorContext);
        System.out.println(outputMessage);
        continue;
      }
      CalcValue value = expr.getValue();
      System.out.println(value.toString());
    }
  }

}
