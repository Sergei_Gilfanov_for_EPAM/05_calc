package calc;

import calc.expr.ExprParser;
import calc.expr.ExprParserInterface;
import calc.expr.ExprTailItemParser;
import calc.factor.FactorParser;
import calc.factor.FactorParserInterface;
import calc.factor.FactorTailItemParser;
import calc.term.TermParser;
import calc.term.TermParserInterface;

public class Parser {
  public ExprParserInterface expr() {
    return new ExprParser();
  }
  public ExprTailItemParser exprTailItem() {
    return new ExprTailItemParser();
  }
  
  public FactorParserInterface factor() {
    return new FactorParser();
  }
  
  public FactorTailItemParser factorTailItem() {
    return new FactorTailItemParser();
  }
  
  public TermParserInterface term() {
    return new TermParser();
  }
}
