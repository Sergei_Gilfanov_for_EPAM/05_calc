package calc.factor;

import java.util.List;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.term.Term;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Абстракция для представления выражения из операторов умножения и деления
 */
public class Factor {
  private Term head;
  private List<FactorTailItem> tail;

  Factor(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException {
    // Это может вызвать, TokenNotFoundException, что означает, что и последовательности множителей
    // factor нет,
    head = parser.term().parse(reader, parser);

    tail = new ArrayList<FactorTailItem>();

    boolean done = false;
    do {
      try {
        FactorTailItem tailItem = parser.factorTailItem().parse(reader, parser);
        tail.add(tailItem);
      } catch (TokenNotFoundException exp) {
        done = true;
      }
    } while (!done);
  }
  
  public CalcValue getValue() {
    CalcValue value = head.getValue();
    for (FactorTailItem tailItem : tail) {
      value = tailItem.applyTo(value);
    }
    return value;
  }


}
