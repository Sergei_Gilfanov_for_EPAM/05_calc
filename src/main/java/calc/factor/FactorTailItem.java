package calc.factor;

import calc.OperatorTailItem;
import calc.term.Term;

/*
 * Абстракция для представления элементов 'хвоста' выражения из операторов умножения и деления
 * Например, 2*3/4 будут созданы элементы "*3", "/4" Как представляются сами выражения и их 'головы'
 * - смотри класс Factor
 */
abstract class FactorTailItem extends OperatorTailItem {
  protected Term term;

  protected FactorTailItem(Term term) {
    this.term = term;
  }

}
