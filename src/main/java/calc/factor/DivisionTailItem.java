package calc.factor;

import calc.CalcValue;
import calc.term.Term;

class DivisionTailItem extends FactorTailItem {

  DivisionTailItem(Term term) {
    super(term);
  }

  @Override
  public CalcValue applyTo(CalcValue head) {
    return head.divide(this.term.getValue());
  }
}
