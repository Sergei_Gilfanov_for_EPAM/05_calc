package calc.factor;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.term.Term;

public class FactorTailItemParser {
  FactorTailItem parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c;
    while (Character.isWhitespace(c = reader.read()));

    Term term;
    switch (c) {
      case '*':
      case '/':
        try {
          term = parser.term().parse(reader, parser);
        } catch (TokenNotFoundException exp) {
          String errorMessage = "Ожидается множитель или делитель";
          throw new ParseErrorException(errorMessage, exp);
        }
        break;
      default:
        reader.unread(c);
        throw new TokenNotFoundException();
    }

    FactorTailItem retval;
    switch (c) {
      case '*':
        retval = new MultiplicationTailItem(term);
        break;
      case '/':
        retval = new DivisionTailItem(term);
        break;
      default:
        // Этот случай уже должен быть отловлен предыдущем switch
        throw new RuntimeException("Unknown operator in Factor");
    }
    return retval;
  }
}
