package calc.factor;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;


public interface FactorParserInterface {

  Factor parse(CalcReader reader, Parser parse) throws IOException, ParseErrorException, TokenNotFoundException;

}
