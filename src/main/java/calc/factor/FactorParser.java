package calc.factor;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;

public class FactorParser implements FactorParserInterface {
  /* (non-Javadoc)
   * @see calc.factor.FactorParserInterface#parse(calc.CalcReader)
   */
  @Override
  public Factor parse(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException {
    return new Factor(reader, parser);
  }
}
