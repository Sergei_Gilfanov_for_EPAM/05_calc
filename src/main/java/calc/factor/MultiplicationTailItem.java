package calc.factor;

import calc.CalcValue;
import calc.term.Term;

class MultiplicationTailItem extends FactorTailItem {

  MultiplicationTailItem(Term term) {
    super(term);
  }

  @Override
  public CalcValue applyTo(CalcValue head) {
    return head.multiply(this.term.getValue());
  }
}
