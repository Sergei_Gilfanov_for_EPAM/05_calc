package calc;

public class ParseErrorException extends Exception {
  public ParseErrorException(String s) {
    super(s);
  }

  public ParseErrorException(String s, Throwable t) {
    super(s, t);
  }
}
