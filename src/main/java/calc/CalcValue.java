package calc;

import java.io.IOException;

public class CalcValue {
  // This class represent immutable value. Don't change after construction.
  private double value;

  public CalcValue(double aValue) {
    value = aValue;
  }

  CalcValue(CalcValue aValue) {
    value = aValue.value;
  }

  public CalcValue(CalcReader reader) throws IOException, ParseErrorException, TokenNotFoundException {
    parse(reader);
  }
  
  public double getValue() {
    return value;
  }

  public CalcValue add(CalcValue b) {
    return new CalcValue(value + b.value);
  }

  public CalcValue subtract(CalcValue b) {
    return new CalcValue(value - b.value);
  }

  public CalcValue multiply(CalcValue b) {
    return new CalcValue(value * b.value);
  }

  public CalcValue divide(CalcValue b) {
    return new CalcValue(value / b.value);
  }

  public boolean equals(CalcValue b) {
    return value == b.value;
  }

  public boolean equals(double b) {
    return value == b;
  }

  public String toString() {
    return Double.toString(value);
  }

  void parse(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    StringBuilder numberAsStringBuilder = new StringBuilder();
    int c;
    boolean parseError = false;
    while (true) {
      // Сейчас будем несколько раз читать символы и возвращать их обратно,
      // что, возможно, снижает эффективность. Но так понятнее, что происходит потом

      // Проверка случая, когда вход начинается с чего-нибудь совершенно на
      // число не похожее
      c = reader.skipWhitespacesRead();
      reader.unread(c);
      if (c != '+' && c != '-' && c != '.' && !Character.isDigit(c)) {
        break;
      }

      // '+' и '-' будем считать частью числа только если они написаны слитно
      int c1 = reader.read(); // пробельные символы уже пропущены выше, поэтому просто read.
      int c2 = reader.read();
      reader.unread(c2);
      reader.unread(c1);
      if ((c1 == '-' || c1 == '+') && !Character.isDigit(c2) && c2 != '.' ) {
        break;
      }

      // Теперь последовательно перекидываем в буфер все, что может представлять собой число
      // Знак мантиссы
      c = reader.read();
      if (c == '+' || c == '-') {
        numberAsStringBuilder.append((char) c);
      } else {
        reader.unread(c);
      }
      // Целая часть мантиссы
      while (Character.isDigit(c = reader.read())) {
        numberAsStringBuilder.append((char) c);
      }

      // Дробная часть, если есть
      if (c == '.') {
        numberAsStringBuilder.append((char) c);
        while (Character.isDigit(c = reader.read())) {
          numberAsStringBuilder.append((char) c);
        }
      }
      // Если нет символа экспоненты, останавливаем парсинг
      if (c != 'e' && c != 'E') {
        reader.unread(c);
        break;
      }

      numberAsStringBuilder.append((char) c);
      // знак экспоненты, если есть
      c = reader.read();
      if (c != '+' && c != '-' && !Character.isDigit(c)) {
        // После символа экспоненты стоит что-то странное - числом это являться не может
        parseError = true;
        reader.unread(c);
        break;
      }
      numberAsStringBuilder.append((char) c);
      // и остаток экспоненты (первый символ мог добавиться на предыдущем шаге)
      while (Character.isDigit(c = reader.read())) {
        numberAsStringBuilder.append((char) c);
      }
      reader.unread(c);
      break;
    }
    if (numberAsStringBuilder.length() == 0) {
      throw new TokenNotFoundException();
    }
    String numberAsString = numberAsStringBuilder.toString();
    String errorMessage = String.format("Неверный формат числа '%s'", numberAsString);
    if (parseError) {
      throw new ParseErrorException(errorMessage);
    }


    try {
      value = Double.parseDouble(numberAsString);
    } catch (NumberFormatException exp) {
      // По идее, если мы все делали правильно, то данная ветка недостижима - ошибки
      // разбора числа случиться не может.
      parseError = true;
      throw new ParseErrorException(errorMessage, exp);
    }
  }
}
