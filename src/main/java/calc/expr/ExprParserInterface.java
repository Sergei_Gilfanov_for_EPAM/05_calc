package calc.expr;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;


public interface ExprParserInterface {

  Expr parse(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException;

}
