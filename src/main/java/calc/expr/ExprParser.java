package calc.expr;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;

public class ExprParser implements ExprParserInterface {
  /* (non-Javadoc)
   * @see calc.expr.ExprParserInterface#parse(calc.CalcReader)
   */
  @Override
  public Expr parse(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException {
    return new Expr(reader, parser);
  }
}
