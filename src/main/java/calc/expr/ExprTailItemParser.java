package calc.expr;

import java.io.IOException;

import calc.CalcReader;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.factor.Factor;

public class ExprTailItemParser {
  ExprTailItem parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c;
    while (Character.isWhitespace(c = reader.read()));

    Factor factor;
    switch (c) {
      case '+':
      case '-':
        try {
          factor = parser.factor().parse(reader, parser);
        } catch (TokenNotFoundException exp) {
          String errorMessage = "Ожидается слагаемое";
          throw new ParseErrorException(errorMessage, exp);
        }
        break;
      default:
        reader.unread(c);
        throw new TokenNotFoundException();
    }

    ExprTailItem retval;
    switch (c) {
      case '+':
        retval = new AdditionTailItem(factor);
        break;
      case '-':
        retval = new SubtractionTailItem(factor);
        break;
      default:
        // Этот случай уже должен быть отловлен предыдущем switch
        throw new RuntimeException("Unknown operator in Expr");
    }
    return retval;
  }
}
