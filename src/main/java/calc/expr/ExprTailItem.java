package calc.expr;

import calc.OperatorTailItem;
import calc.factor.Factor;

/*
 * Абстракция для представления элементов 'хвоста' выражения из операторов сложения в вычитания для
 * 1 + 2 - 3 + 4 будут созданы элементы "+2", "-3", "+4",
 */
public abstract class ExprTailItem extends OperatorTailItem {
  Factor factor;

  ExprTailItem(Factor aFactor) {
    factor = aFactor;
  }

}
