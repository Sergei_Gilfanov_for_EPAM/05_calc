package calc.expr;

import calc.CalcValue;
import calc.factor.Factor;

public class SubtractionTailItem extends ExprTailItem {

  SubtractionTailItem(Factor aFactor) {
    super(aFactor);
  }

  public CalcValue applyTo(CalcValue head) {
    return head.subtract(this.factor.getValue());
  }
}
