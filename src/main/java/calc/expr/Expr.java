package calc.expr;

import java.util.List;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.factor.Factor;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Абстракция для представления выражения из операторов сложения и вычитания.
 */
public class Expr {
  Factor head;
  List<ExprTailItem> tail;

  Expr(CalcReader reader, Parser parser) throws IOException, ParseErrorException, TokenNotFoundException {
    // нас нет,
    try {
      head = parser.factor().parse(reader, parser);
    } catch (TokenNotFoundException exp) {
      head = null;
    }

    tail = new ArrayList<ExprTailItem>();

    boolean done = false;
    do {
      try {
        ExprTailItem tailItem = parser.exprTailItem().parse(reader, parser);
        tail.add(tailItem);
      } catch (TokenNotFoundException exp) {
        done = true;
      }
    } while (!done);

    // Тут пользуемся знанием о всей грамматике - выражение может оканчиваться только на скобку или
    // конец ввода
    int c;
    while (Character.isWhitespace(c = reader.read()));
    if (c != ')' && c != -1) {
      // нужно для правильного вывода ошибки, иначе в контексте будет выведен и
      // последний (неправильный) символ
      reader.unread(c);
      throw new ParseErrorException("Ожидается слагаемое");
    }

    if (head == null && tail.size() == 0) {
      reader.unread(c);
      throw new TokenNotFoundException();
    }
    reader.unread(c);
  }


  public CalcValue getValue() {
    CalcValue value;
    if (head == null) {
      value = new CalcValue(0);
    } else {
      value = head.getValue();
    }
    for (ExprTailItem tailItem : tail) {
      value = tailItem.applyTo(value);
    }
    return value;
  }
}
