package calc;

import static org.junit.Assert.*;

import java.io.IOException;

public class ParserStub {
  private String expectedInput;
  CalcValue fakeValue;
  
  ParserStub(String expectedInput, CalcValue fakeValue) {
    this.expectedInput = expectedInput;
    this.fakeValue = fakeValue;
  }

  public CalcValue getValue() {
    return fakeValue;
  }
  
  void FakeParseAndVerify(CalcReader reader) throws IOException, TokenNotFoundException, ParseErrorException {
    StringBuilder parsed = new StringBuilder();
    for (int i = expectedInput.length(); i > 0; --i) {
      int c = reader.read();
      if (c < 0) {
        String errorMessage =
            String.format("Parser input error. Expecting '%s'. Got '%s'<EOF> instead",
                expectedInput, parsed.toString());
        fail(errorMessage);
      }
      if ( c == '$') {
        throw new TokenNotFoundException();
      }
      if ( c == '!') {
        throw new ParseErrorException("Parse error by request");
      }
      parsed.append((char) c);
    }
    String parsedString = parsed.toString();
    if (!expectedInput.equals(parsedString)) {
      String errorMessage = String.format("Parser input error. Expecting '%s'. Got '%s' instead",
          expectedInput, parsedString);
      fail(errorMessage);
    }
  }
}
