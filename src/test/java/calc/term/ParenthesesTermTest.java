package calc.term;

import java.io.IOException;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.CoreMatchers.*;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;


import calc.CalcReaderStub;
import calc.CalcValue;
import calc.ExprParserStub;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.term.ParenthesesTerm;
import calc.term.Term;

@RunWith(MockitoJUnitRunner.class)
public class ParenthesesTermTest {
  private ExprParserStub exprParserStub = new ExprParserStub("))))((((", new CalcValue(277));
  
  @Mock Parser parser;

  @Before
  public void setUp() {
    when(parser.expr()).thenReturn(exprParserStub);
  }
  
  @Test
  public void valueIsEqualOfInnerExpression()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "())))(((()asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    Term term = new ParenthesesTerm(reader, parser);
    CalcValue value = term.getValue();
    assertThat(value, equalTo(exprParserStub.getValue()));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void shouldSkipSpacesAtStart()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "    ())))(((()asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    Term term = new ParenthesesTerm(reader, parser);
    CalcValue value = term.getValue();
    assertThat(value, equalTo(exprParserStub.getValue()));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void shouldSkipSpacesAtEnd()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "())))((((    )asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    Term term = new ParenthesesTerm(reader, parser);
    CalcValue value = term.getValue();
    assertThat(value, equalTo(exprParserStub.getValue()));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void shouldDieWhenNoOpenParenthess()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "#))))(((()asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    try {
      new ParenthesesTerm(reader, parser);
      fail(String.format("Parse error was not thrown when parsing %s", termAsString));
    } catch (TokenNotFoundException ex) {
      reader.assertUnparsed("#))))(((()asdf");
    }
  }

  @Test
  public void shouldDieWhenNoCloseParenthess()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "())))(((( #)asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    try {
      new ParenthesesTerm(reader, parser);
      fail(String.format("Parse error was not thrown when parsing %s", termAsString));
    } catch (ParseErrorException ex) {
      reader.assertUnparsed("#)asdf");
    }
  }

  @Test
  public void shouldDieWhenNoExpression()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String termAsString = "($)asdf";
    CalcReaderStub reader = new CalcReaderStub(termAsString);
    try {
      new ParenthesesTerm(reader, parser);
      fail(String.format("Parse error was not thrown when parsing %s", termAsString));
    } catch (ParseErrorException ex) {
      reader.assertUnparsed(")asdf");
    }
  }
}
