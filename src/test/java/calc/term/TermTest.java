package calc.term;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.term.Term;

public class TermTest {
  Parser parser = new Parser();
 
  @Test
  public void testParseTermNumber()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-12e5";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = parser.term().parse(reader, parser);
    assertTrue("Parsing", term instanceof NumberTerm);
  }

  @Test
  public void testParseTermNumberValue()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-12e5";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = parser.term().parse(reader, parser);
    assertTrue("Parsing", term.getValue().equals(-12e5));
  }

  @Test
  public void testParseTermParentheses()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "(1)";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = parser.term().parse(reader, parser);
    assertTrue("Parsing", term instanceof ParenthesesTerm);
  }

  @Test
  public void testParseTermParenthesesValue()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "(1 + 2*3)";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = parser.term().parse(reader, parser);
    assertTrue("Parsing", term.getValue().equals(7));
  }

  @Test(expected = TokenNotFoundException.class)
  public void testParseTermNoTerm()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = " ";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = parser.term().parse(reader, parser); // execution will not pass this line
    CalcValue value = term.getValue();
    fail(String.format("TokenNotFound error not catched, got %f", value));
  }

}
