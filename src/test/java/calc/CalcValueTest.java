package calc;

import static org.junit.Assert.*;
import java.io.IOException;

import org.junit.Test;

public class CalcValueTest {
  static String[] doubleLiterals = {".2", ".2e3", ".2e-3", ".2e+3", "1", "1e3", "1e-3", "1e+3",
      "1.", "1.e3", "1.e-3", "1.e+3", "1.2", "1.2e3", "1.2e-3", "1.2e+3", "-.2", "-.2e3", "-.2e-3",
      "-.2e+3", "-1", "-1e3", "-1e-3", "-1e+3", "-1.", "-1.e3", "-1.e-3", "-1.e+3", "-1.2",
      "-1.2e3", "-1.2e-3", "-1.2e+3", "+.2", "+.2e3", "+.2e-3", "+.2e+3", "+1", "+1e3", "+1e-3",
      "+1e+3", "+1.", "+1.e3", "+1.e-3", "+1.e+3", "+1.2", "+1.2e3", "+1.2e-3", "+1.2e+3"};

  @Test
  public void shouldGiveRightValue()
      throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : doubleLiterals) {
      CalcReader reader = new CalcReaderStub(valueAsString);
      String assertMessage = String.format("Parsing %s", valueAsString);
      CalcValue value = new CalcValue(reader);
      double requiredValue = Double.parseDouble(valueAsString);
      assertTrue(assertMessage, value.equals(requiredValue));
    }
  }

  @Test
  public void shouldStopParsingAtSpace()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2e3 45"
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s 45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed(" 45");
    }
  }

  @Test
  public void shouldStopParsingAtAdd()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2e3+45"
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s+45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed("+45");
    }
  }

  @Test
  public void shouldStopParsingAtSubtract()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2e3-45"
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s-45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed("-45");
    }
  }

  @Test
  public void shouldStopParsingAtDot()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2.45"
    for (String valueAsString : doubleLiterals) {
      // Те литералы, в которых нет точки, пропускаем, тк, например
      // +1 после добавления тестового суффикса даст нам 1.45, что распарсится целиком
      if (valueAsString.indexOf('.') < 0) {
        continue;
      }
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s.45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed(".45");
    }
  }

  @Test
  public void shouldStopParsingAtEAfterNumber()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // 1.2e3e45
    for (String valueAsString : doubleLiterals) {
      // Те литералы, в которых нет 'e', пропускаем, тк, например
      // +1.2 после добавления тестового суффикса даст нам 1.2e45, что распарсится целиком
      if (valueAsString.indexOf('e') < 0) {
        continue;
      }
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%se45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed("e45");
    }
  }

  @Test
  public void shouldStopParsingAtOtherChars()
      throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2e3какой-то мусор"
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%sq45", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      CalcValue value = new CalcValue(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, value.equals(requiredValue));
      reader.assertUnparsed("q45");
    }
  }

  @Test
  public void shouldDieAtWrongE() throws IOException, ParseErrorException, TokenNotFoundException {
    // 1.2eкакой-то мусор
    for (String valueAsString : doubleLiterals) {
      // Те литералы, в которых есть 'e', пропускаем, в них экспонента заведомо правильная
      if (valueAsString.indexOf('e') >= 0) {
        continue;
      }
      String valueWithTail = String.format("%se(w", valueAsString);
      CalcReaderStub reader = new CalcReaderStub(valueWithTail);
      try {
        CalcValue value = new CalcValue(reader);
        fail(String.format("Parse error was not thrown when parsing %s, got %f instead",
            valueAsString, value.getValue()));
      } catch (ParseErrorException ex) {
        reader.assertUnparsed("(w");
      }
    }
  }

  static String[] operatorLiterals = {"+ ", "+(1", "+q1", "- ", "-(1", "-q1"};

  @Test
  public void shouldDieAtUnaryOperator()
      throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : operatorLiterals) {
      CalcReaderStub reader = new CalcReaderStub(valueAsString);
      try {
        CalcValue value = new CalcValue(reader);
        fail(String.format("Parse error was not thrown when parsing %s, got %f instead",
            valueAsString, value.getValue()));
      } catch (TokenNotFoundException ex) {
        reader.assertUnparsed(valueAsString);
      }
    }
  }

  @Test
  public void shouldDieAtGarbage() throws IOException, ParseErrorException, TokenNotFoundException {
    // ex: "1.2e3какой-то мусор"
    String valueAsString = "qwe";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      CalcValue value = new CalcValue(reader);
      fail(String.format("Parse error was not thrown when parsing %s, got %f instead",
          valueAsString, value.getValue()));
    } catch (TokenNotFoundException ex) {
      reader.assertUnparsed(valueAsString);
    }
  }
}
