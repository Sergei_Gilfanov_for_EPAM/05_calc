package calc;

import java.io.IOException;

import org.mockito.Mockito;

import static org.mockito.Mockito.*;

import calc.expr.Expr;
import calc.expr.ExprParserInterface;

public class ExprParserStub extends ParserStub implements ExprParserInterface {

  public ExprParserStub(String expectedInput, CalcValue fakeValue) {
    super(expectedInput, fakeValue);
  }

  @Override
  public Expr parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    FakeParseAndVerify(reader);
    
    Expr retval = Mockito.mock(Expr.class);
    when(retval.getValue()).thenReturn(getValue());
    // TODO Auto-generated method stub
    return retval;
  }

}
