package calc;

import static org.junit.Assert.*;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;

public class CalcReaderStub extends CalcReader {

  public CalcReaderStub(String s) {
    super(s);
  }

  public void assertUnparsed(String toMatch) throws IOException {
    StringBuilder dataInReader = new StringBuilder();
    int c;
    while ((c = this.read()) > 0) {
      dataInReader.append((char) c);
    }
    assertThat(dataInReader.toString(), is(toMatch));
  }
}
