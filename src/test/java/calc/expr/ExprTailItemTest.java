package calc.expr;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import calc.CalcReaderStub;
import calc.CalcValue;
import calc.FactorParserStub;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.expr.AdditionTailItem;
import calc.expr.ExprTailItem;
import calc.expr.SubtractionTailItem;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExprTailItemTest {
  FactorParserStub factorParserStub = new FactorParserStub("########", new CalcValue(337));

  @Mock Parser parser;
  
  @Before
  public void setUp() {
    // Используется внутри exprTailItemParse
    when(parser.factor()).thenReturn(factorParserStub);
    // А сам exprTailItemParser мы тестируем
    when(parser.exprTailItem()).thenReturn(new ExprTailItemParser());
  } 
  
  @Test
  public void testParseExprTailItemAddType()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "+########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    ExprTailItem item = parser.exprTailItem().parse(reader, parser);
    assertTrue("'+...' should give AdditionTailItem", item instanceof AdditionTailItem);
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseExprTailItemMultApply()
      throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);

    String valueAsString = "+########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    ExprTailItem item = parser.exprTailItem().parse(reader, parser);
    CalcValue res = item.applyTo(head);
    CalcValue expectedRes = head.add(factorParserStub.getValue());
    assertTrue("AdditionTailItem: wrong calculations", res.equals(expectedRes));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseExprTailItemSubtractionType()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    ExprTailItem item = parser.exprTailItem().parse(reader, parser);
    assertTrue("'+...' should give SubtractionTailItem", item instanceof SubtractionTailItem);
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseExprTailItemSubtractApply()
      throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);

    String valueAsString = "-########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    ExprTailItem item = parser.exprTailItem().parse(reader, parser);
    CalcValue res = item.applyTo(head);
    CalcValue expectedRes = head.subtract(factorParserStub.getValue());    
    assertTrue("SubtractionTailItem: wrong calculations", res.equals(expectedRes));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseExprTailItemNoFactor()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-$########";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      parser.exprTailItem().parse(reader, parser);
    } catch (ParseErrorException ex) {
      reader.assertUnparsed("########");
    }
  }

  @Test
  public void testParseExprTailItemNoExprTail()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = " !########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      parser.exprTailItem().parse(reader, parser);
    } catch (TokenNotFoundException ex) {
      reader.assertUnparsed("!########asdf");
    }
  }
}
