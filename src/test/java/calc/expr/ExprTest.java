package calc.expr;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TokenNotFoundException;
import calc.expr.Expr;

public class ExprTest {
  Parser parser = new Parser();
  
  @Test(expected = TokenNotFoundException.class)
  public void testEmpty() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser); // execution will not pass this line
    CalcValue value = expr.getValue();
    fail(String.format("Parse error not catched got %f, got %f", value));
  }

  @Test
  public void testNumber() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "1.2e5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(1.2e5));
  }

  @Test
  public void testPositiveNumber() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "+1.2e5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(+1.2e5));
  }

  @Test
  public void testNegativeNumber() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "-1.2e5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Negative integer", value.equals(-1.2e5));
  }

  @Test
  public void testUnaryPlus() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "+ 1.2e5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(1.2e5));
  }

  @Test
  public void testUnaryPlusInParenthesses()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "1 + (+ 1.2e5)";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(1 + (+1.2e5)));
  }

  @Test
  public void testUnaryMinus() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "- 1.2e5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(-1.2e5));
  }

  @Test
  public void testUnaryMinusInParenthesses()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "1 + (- 1.2e5)";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Integer", value.equals(1 + (-1.2e5)));
  }

  @Test
  public void testAssociativity() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "-2*3 + 4*5";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Associativity test", value.equals(-2 * 3 + 4 * 5));
  }

  @Test
  public void testNoHead() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "+ 4*5 -2*3 ";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser); // execution will not pass this line
    CalcValue value = expr.getValue();
    assertTrue("NoHead", value.equals(+4 * 5 - 2 * 3));
  }

  @Test(expected = ParseErrorException.class)
  public void testGarbageAtEnd() throws IOException, ParseErrorException, TokenNotFoundException {
    // должно распарсить 2*3 + 6/2.2, до .2*3
    String exprAsString = "2*3 + 6/2.2 2*3";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser); // execution will not pass this line
    CalcValue value = expr.getValue();
    fail(String.format("Parse error not catched got %f", value));
  }

  @Test(expected = ParseErrorException.class)
  public void testParseError() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "2*3 + 4*5 - ";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser); // execution will not pass this line
    CalcValue value = expr.getValue();
    fail(String.format("Parse error not catched got %f", value));
  }

  @Test(expected = ParseErrorException.class)
  public void testGarbageInput() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "й+12";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    fail(String.format("Parse error not catched got %f, got %f", value));
  }

  @Test
  public void testSignInExponent() throws IOException, ParseErrorException, TokenNotFoundException {
    // Должно интерпретироваться как сумма двух чисел, а не трех
    String exprAsString = "-2e+1 + 6";
    CalcReader reader = new CalcReader(exprAsString);
    Expr expr = parser.expr().parse(reader, parser);
    CalcValue value = expr.getValue();
    assertTrue("Exponent", value.equals(-2e+1 + 6));
  }

}
