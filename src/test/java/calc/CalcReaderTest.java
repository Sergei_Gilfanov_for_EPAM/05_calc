package calc;

import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class CalcReaderTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void shouldReadFromStart() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    assertThat(c, is((int) 'a'));
  }

  @Test
  public void shouldCatchEOF() throws IOException {
    CalcReader reader = new CalcReader("a");
    int c = reader.read();
    c = reader.read();
    assertThat(c, is(-1));
  }

  @Test
  public void shouldNotSkipSpaces() throws IOException {
    CalcReader reader = new CalcReader(" as");
    int c = reader.skipWhitespacesRead();
    assertThat(c, is((int) 'a'));
  }

  @Test
  public void shouldSkipSpaces() throws IOException {
    CalcReader reader = new CalcReader(" as");
    int c = reader.read();
    assertThat(c, is((int) ' '));
  }

  @Test
  public void shouldUnreadChars() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    reader.unread('b');
    c = reader.read();
    assertThat(c, is((int) 'b'));
  }

  @Test
  public void shouldUnreadSpaces() throws IOException {
    CalcReader reader = new CalcReader(" as");
    int c = reader.read();
    reader.unread(' ');
    c = reader.read();
    assertThat(c, is((int) ' '));
  }

  @Test
  public void unreadShouldIgnoreEOF() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    reader.unread(-1);
    c = reader.read();
    assertThat(c, is((int) 's'));
  }

  @Test
  public void unreadWithoutReadingShouldBeSafe() throws IOException {
    CalcReader reader = new CalcReader("as");
    reader.unread('c');
    int c = reader.read();
    assertThat(c, is((int) 'c'));
  }

  @Test
  public void shouldPeek() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    c = reader.peek();
    assertThat(c, is((int) 's'));

  }

  @Test
  public void shouldGiveReadingContext() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    String context = reader.getContext();
    assertThat(context, is("12"));
  }

  @Test
  public void unreadShouldAffectReadingContext() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    reader.unread('3');
    String context = reader.getContext();
    assertThat(context, is("1"));
  }

  @Test
  public void contextShouldWorkAfterUnread() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    reader.unread('3');
    reader.read();
    String context = reader.getContext();
    assertThat(context, is("13"));
  }

  @Test
  public void contextShouldSlide() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    for (int i = 16; i > 0; i--) {
      reader.read();
    }
    String context = reader.getContext();
    assertThat(context, is("23456789abcdefg"));
  }
}
