package calc.factor;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

import calc.CalcReader;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.Testutil;
import calc.TokenNotFoundException;
import calc.factor.Factor;

public class FactorTest {
  Parser parser = new Parser();
  
  @Test
  public void testOneTerm() throws IOException, ParseErrorException, TokenNotFoundException {
    String factorAsString = "-12e5";
    CalcReader reader = new CalcReader(factorAsString);
    Factor factor = new Factor(reader, parser);
    CalcValue value = factor.getValue();
    assertTrue("One term", value.equals(-12e5));
  }

  @Test
  public void testAssociativity() throws IOException, ParseErrorException, TokenNotFoundException {
    String factorAsString = "6/2*3";
    CalcReader reader = new CalcReader(factorAsString);
    Factor factor = new Factor(reader, parser);
    CalcValue value = factor.getValue();
    assertTrue("Associativity test", value.equals(9));
  }

  @Test
  public void testParseStop() throws IOException, ParseErrorException, TokenNotFoundException {
    // должно распарсить 6/2, до .2*3
    String factorAsString = "6/2.2.2*3";
    CalcReader reader = new CalcReader(factorAsString);
    Factor factor = new Factor(reader, parser);
    CalcValue value = factor.getValue();
    String assertMessage = String.format("Parsing %s", factorAsString);
    assertTrue(assertMessage, Testutil.valueAndRest(value, 6 / 2.2, reader, ".2*3"));
  }

  @Test(expected = ParseErrorException.class)
  public void testParseError() throws IOException, ParseErrorException, TokenNotFoundException {
    String exprAsString = "2*3/";
    CalcReader reader = new CalcReader(exprAsString);
    Factor factor = new Factor(reader, parser); // execution will not pass this line
    CalcValue value = factor.getValue();
    fail(String.format("Parse error not catched, got %f", value));
  }

  @Test(expected = TokenNotFoundException.class)
  public void testNoFactor() throws IOException, ParseErrorException, TokenNotFoundException {
    String factorAsString = "й";
    CalcReader reader = new CalcReader(factorAsString);
    Factor factor = new Factor(reader, parser);
    CalcValue value = factor.getValue();
    fail(String.format("TokenNotFound error not catched, got %f", value));
  }

  @Test
  public void testStopAfterFactor()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String factorAsString = "6/2*3+4";
    CalcReader reader = new CalcReader(factorAsString);
    Factor factor = new Factor(reader, parser);
    CalcValue value = factor.getValue();
    String assertMessage = String.format("Parsing %s", factorAsString);
    assertTrue(assertMessage, Testutil.valueAndRest(value, 6 / 2 * 3, reader, "+4"));
  }
}
