package calc.factor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import calc.CalcReaderStub;
import calc.CalcValue;
import calc.ParseErrorException;
import calc.Parser;
import calc.TermParserStub;
import calc.TokenNotFoundException;
import calc.factor.DivisionTailItem;
import calc.factor.FactorTailItem;
import calc.factor.MultiplicationTailItem;

@RunWith(MockitoJUnitRunner.class)
public class FactorTailItemTest {
  TermParserStub termParserStub = new TermParserStub("########", new CalcValue(137));
  
  @Mock Parser parser;
  
  @Before
  public void setUp() {
    // Используется внутри factorTailItem
    when(parser.term()).thenReturn(termParserStub);
    // А сам FactorTailItem мы тестируем
    when(parser.factorTailItem()).thenReturn(new FactorTailItemParser());
  }  

  @Test
  public void testParseFactorTailItemMultType()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "*########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    FactorTailItem item = parser.factorTailItem().parse(reader, parser);
    assertTrue("'*...' should give MultiplicationTailItem", item instanceof MultiplicationTailItem);
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseFactorTailItemMultApply()
      throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);

    String valueAsString = "*########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    FactorTailItem item = parser.factorTailItem().parse(reader, parser);
    CalcValue res = item.applyTo(head);
    CalcValue expectedRes = head.multiply(termParserStub.getValue());
    assertTrue("MultiplicationTailItem: wrong calculations", res.equals(expectedRes));
    reader.assertUnparsed("asdf");
  }


  @Test
  public void testParseFactorTailItemDivType()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "/########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    FactorTailItem item = parser.factorTailItem().parse(reader, parser);
    assertTrue("'/....' should give DivisionTailItem", item instanceof DivisionTailItem);
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseFactorTailItemDivApply()
      throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(-6);

    String valueAsString = "/########asdf";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    FactorTailItem item = parser.factorTailItem().parse(reader, parser);
    CalcValue res = item.applyTo(head);
    CalcValue expectedRes = head.divide(termParserStub.getValue());
    assertTrue("DivisionTailItem: wrong calculations", res.equals(expectedRes));
    reader.assertUnparsed("asdf");
  }

  @Test
  public void testParseFactorTailItemNoTerm()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "/$########";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      parser.factorTailItem().parse(reader, parser);
    } catch (ParseErrorException ex) {
      reader.assertUnparsed("########");
    }
  }

  @Test
  public void testParseFactorTailItemNoFactorTail()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "$########";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      parser.factorTailItem().parse(reader, parser);
    } catch (TokenNotFoundException ex) {
      reader.assertUnparsed("$########");
    }
  }
  
  @Test
  public void shouldSkipSpaces()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "   /########";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    parser.factorTailItem().parse(reader, parser);
  }
  
  @Test
  public void shouldDieOnGarbage()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "asdf########";
    CalcReaderStub reader = new CalcReaderStub(valueAsString);
    try {
      parser.factorTailItem().parse(reader, parser);
    } catch (TokenNotFoundException ex) {
      reader.assertUnparsed("asdf########");
    }
  }
}
