package calc;

import static org.mockito.Mockito.when;

import java.io.IOException;

import org.mockito.Mockito;

import calc.factor.Factor;
import calc.factor.FactorParserInterface;

public class FactorParserStub extends ParserStub implements FactorParserInterface {


  public FactorParserStub(String expectedInput, CalcValue fakeValue) {
    super(expectedInput, fakeValue);
  }

  @Override
  public Factor parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    FakeParseAndVerify(reader);
    
    Factor retval = Mockito.mock(Factor.class);
    when(retval.getValue()).thenReturn(getValue());
    return retval;
  }
  
}
