package calc;

import static org.mockito.Mockito.when;

import java.io.IOException;

import org.mockito.Mockito;

import calc.term.Term;
import calc.term.TermParserInterface;

public class TermParserStub extends ParserStub implements TermParserInterface {

  public TermParserStub(String expectedInput, CalcValue fakeValue) {
    super(expectedInput, fakeValue);
  }

  @Override
  public Term parse(CalcReader reader, Parser parser)
      throws IOException, ParseErrorException, TokenNotFoundException {
    FakeParseAndVerify(reader);
    
    Term retval = Mockito.mock(Term.class);
    when(retval.getValue()).thenReturn(getValue());
    return retval;
  }

}
